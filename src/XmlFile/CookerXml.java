/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */
@XmlRootElement(name = "cooker")
@XmlAccessorType(XmlAccessType.FIELD)
public class CookerXml {
       @XmlElement(name = "reservation")
    private List<CookerDashboardXml> cookerDashboardXml;

    public List<CookerDashboardXml> getCookerDashboardXml() {
        return cookerDashboardXml;
    }

    public void setCookerDashboardXml(List<CookerDashboardXml> cookerDashboardXml) {
        this.cookerDashboardXml = cookerDashboardXml;
    }
    
}
