/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */
@XmlRootElement(name = "manager")
@XmlAccessorType(XmlAccessType.FIELD)
public class ManagerXml {
    @XmlElement(name = "reservations")
    private List<ManagerDashboardXml> managerDashborad;

    public List<ManagerDashboardXml> getManagerDashborad() {
        return managerDashborad;
    }

    public void setManagerDashborad(List<ManagerDashboardXml> managerDashborad) {
        this.managerDashborad = managerDashborad;
    }
    
}
