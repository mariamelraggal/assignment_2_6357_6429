/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */

@XmlRootElement(name = "tables")
@XmlAccessorType(XmlAccessType.FIELD)
public class TablesXml {
     @XmlElement( name = "table")
    private List<TableXml> tables;

    public List<TableXml> getTables() {
        return tables;
    }

    public void setTables(List<TableXml> tables) {
        this.tables = tables;
    }
     
}
