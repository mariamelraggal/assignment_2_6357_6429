/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */
@XmlRootElement(name = "dashboards")
@XmlAccessorType(XmlAccessType.FIELD)
public class Dashboards {
    @XmlElement(name = "waiter")
    private WaiterXml waiterXml=null;
    @XmlElement(name = "cooker")
    private CookerXml cookerXml=null;
    @XmlElement(name = "manager")
    private ManagerXml managerXml=null;
    
    public WaiterXml getWaiterXml() {
        return waiterXml;
    }

    public void setWaiterXml(WaiterXml waiterXml) {
        this.waiterXml = waiterXml;
    }

    public CookerXml getCookerXml() {
        return cookerXml;
    }

    public void setCookerXml(CookerXml cookerXml) {
        this.cookerXml = cookerXml;
    }

    public ManagerXml getManagerXml() {
        return managerXml;
    }

    public void setManagerXml(ManagerXml managerXml) {
        this.managerXml = managerXml;
    }
    
}
