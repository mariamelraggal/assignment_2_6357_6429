/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */
@XmlRootElement(name = "waiter")
@XmlAccessorType(XmlAccessType.FIELD)
public class WaiterXml {
    @XmlElement(name = "reservation")
    List<WaiterDashboardXml> waiterDashboradXml;

    public List<WaiterDashboardXml> getWaiterDashboradXml() {
        return waiterDashboradXml;
    }

    public void setWaiterDashboradXml(List<WaiterDashboardXml> waiterDashboradXml) {
        this.waiterDashboradXml = waiterDashboradXml;
    }
    
}
