/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */

@XmlRootElement(name = "reservation")
@XmlAccessorType(XmlAccessType.FIELD)
public class WaiterDashboardXml {
     @XmlElement(name = "client_name")
    private String personName;
      @XmlElement(name = "table_number")
    private int reservedTableNumber;

    public WaiterDashboardXml(String personName, int reservedTableNumber) {
        this.personName = personName;
        this.reservedTableNumber = reservedTableNumber;
    }
    
    public WaiterDashboardXml(){
    }
    
    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public int getReservedTableNumber() {
        return reservedTableNumber;
    }

    public void setReservedTableNumber(int reservedTableNumber) {
        this.reservedTableNumber = reservedTableNumber;
    }
    
    
}
