/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */
@XmlRootElement(name = "reservation")
@XmlAccessorType(XmlAccessType.FIELD)
public class CookerDashboardXml {
   @XmlElement(name = "tablenumber")
    private int numberOfTable;
    @XmlElement(name = "orders")
    private List<String> ordersList;
    @XmlElement(name = "quantity")
    private List<Integer> quantity;

    public CookerDashboardXml(int numberOfTable, List<String> ordersList, List<Integer> quantity) {
        this.numberOfTable = numberOfTable;
        this.ordersList = ordersList;
        this.quantity = quantity;
    }

 

    public CookerDashboardXml() {
    }


    public int getNumberOfTable() {
        return numberOfTable;
    }

    public void setNumberOfTable(int numberOfTable) {
        this.numberOfTable = numberOfTable;
    }

    public List<String> getOrdersList() {
        return ordersList;
    }

    public void setOrdersList(List<String> ordersList) {
        this.ordersList = ordersList;
    }

    public List<Integer> getQuantity() {
        return quantity;
    }

    public void setQuantity(List<Integer> quantity) {
        this.quantity = quantity;
    }
    
}
