/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */
@XmlRootElement(name = "dishes")
@XmlAccessorType(XmlAccessType.FIELD)
public class DishesXml {
     @XmlElement( name = "dish")
    private List<DishXml> dishes;

    public List<DishXml> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishXml> dishes) {
        this.dishes = dishes;
    }
     
}
