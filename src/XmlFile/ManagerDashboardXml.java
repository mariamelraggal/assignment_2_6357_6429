/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package XmlFile;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author LENOVO
 */

@XmlRootElement(name = "reservations")
@XmlAccessorType(XmlAccessType.FIELD)
public class ManagerDashboardXml {
    @XmlElement(name = "client_name")
    private String nameOfPerson;
    @XmlElement(name = "tablenumber")    
    private int tableNumberManager;
    @XmlElement(name = "orders")
    private List<String> orderedDishes;
    @XmlElement(name = "quantity")
    private List<Integer> dishesQuantity;
    @XmlElement(name = "price")
    private double amount;

    public ManagerDashboardXml(String nameOfPerson, int tableNumberManager, List<String> orderedDishes, List<Integer> dishesQuantity, double amount) {
        this.nameOfPerson = nameOfPerson;
        this.tableNumberManager = tableNumberManager;
        this.orderedDishes = orderedDishes;
        this.dishesQuantity = dishesQuantity;
        this.amount = amount;
    }

    public ManagerDashboardXml() {
    }
    
    
    public String getNameOfPerson() {
        return nameOfPerson;
    }

    public void setNameOfPerson(String nameOfPerson) {
        this.nameOfPerson = nameOfPerson;
    }

    public int getTableNumberManager() {
        return tableNumberManager;
    }

    public void setTableNumberManager(int tableNumberManager) {
        this.tableNumberManager = tableNumberManager;
    }

    public List<String> getOrderedDishes() {
        return orderedDishes;
    }

    public void setOrderedDishes(List<String> orderedDishes) {
        this.orderedDishes = orderedDishes;
    }

    public List<Integer> getDishesQuantity() {
        return dishesQuantity;
    }

    public void setDishesQuantity(List<Integer> dishesQuantity) {
        this.dishesQuantity = dishesQuantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
    
    
}
