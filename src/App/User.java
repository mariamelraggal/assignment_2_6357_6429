/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import XmlFile.DishXml;
import XmlFile.RestaurantXml;
import XmlFile.UserXml;
import XmlFile.UsersXml;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class User {

private PopMessage pop = new PopMessage();
private String username;
private String password;
private String  Name;
private int tableNum;

    public String logIn(TextField usernameText, PasswordField passwordText, RestaurantXml restaurantXml) {
        
        boolean x = false;
        try {
            
            String username = usernameText.getText();
            String password = passwordText.getText();
            for (UserXml user : restaurantXml.getUsers().getUser()) {
                
                if (username.equals(user.getUsername())) {
                    x=true;
                    if (password.equals(user.getPassword())) {
                        x=true;
                        this.Name=user.getName();
                        return user.getRole();
                        
                    }else{
                        x=false;
                    }
                }else{
                    x=false;
                }
            }
            if(!x){
                pop.display3();
            }
        } catch (Exception e) {
            pop.display();
        }
        return null;
    }

    
    public String logIn(){
        System.out.println("REGITSER");
        return "";
    }
    
        public Boolean signUp(TextField name,TextField username,PasswordField ps,RestaurantXml rest,JAXBContext jaxbContext)
    {
        try{
            String n=name.getText();
            String un=username.getText();
            String pass=ps.getText();
            Boolean x=false;
            if(!(name.getText().isEmpty())&& !(username.getText().isEmpty())&& !(ps.getText().isEmpty())){  
                if(pass.length()>=8){
            for(UserXml user : rest.getUsers().getUser())
            {
                if(un.equals(user.getUsername()))
                {
                    x=true;
                    break;
                }
                    
            }
            if(x)
            {
                pop.display10();
            }else
            {
                List<UserXml> user=new ArrayList<>();
                for (UserXml user1 : rest.getUsers().getUser())
                {
                    user.add(new UserXml(user1.getName(),user1.getRole(),user1.getUsername(),user1.getPassword()));
                }
                user.add(new UserXml(n,"Client",un,pass));
                UsersXml usersXml=new UsersXml();
                usersXml.setUser(user);
                rest.setUsers(usersXml);
                        try {
                            Marshaller marshaller = jaxbContext.createMarshaller();
                            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                            OutputStream output = new FileOutputStream("Data.xml");
                            marshaller.marshal(rest, output);
                        } catch (JAXBException ex) {
                            System.out.println("error jaxb catch 1");
                        } catch (FileNotFoundException ex) {
                            System.out.println("error file catch 2");
                        } 
                        return true;
            } 
                }else{
                    pop.display11();
                }
                
            }
            else
            {
                pop.display9();
            }
        }catch(Exception g)
        {
            pop.display9();
        }
        
      return false;  
    }
    
    
    
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTableNum() {
        return tableNum;
    }

    public void setTableNum(int tableNum) {
        this.tableNum = tableNum;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    

   
    
    
}
