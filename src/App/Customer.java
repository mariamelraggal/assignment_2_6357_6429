/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;


import XmlFile.DishXml;
import java.util.List;


/**
 *
 * @author Yosf
 */

public class Customer extends User{
    private double paymentAmount;
    private final double mainCourseTax=0.15;
    private final double desertTax=0.2;
    private final double appetizerTax=0.1;
    
    public Customer() {
        
        this.paymentAmount = 0.0;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
    
    
   public void Checkout(List<String> orderList,List<Integer> quantityList,List<DishXml>dish)
    {      
        this.paymentAmount=0.0;
           for(int k=0;k<orderList.size();k++){
           for(int i=0;i<dish.size();i++)
           {
               DishXml m=dish.get(i);
               String dishname=m.getName();
               if(dishname.equals(orderList.get(k)))
               {
                   String dishtype=m.getType();
                   double dishprice=m.getPrice();
                   switch(dishtype){
                       case"main_course":
                        this.paymentAmount +=(quantityList.get(k))*(dishprice+(dishprice*mainCourseTax));
                           break;
                       case"desert":
                        this.paymentAmount +=(quantityList.get(k))*(dishprice+(dishprice*desertTax));
                            break;
                       case"appetizer":
                      this.paymentAmount +=(quantityList.get(k))*(dishprice+(dishprice*appetizerTax));
                          break;
                   }
                       
               }
 
           }
           }
        
    }
    
}
