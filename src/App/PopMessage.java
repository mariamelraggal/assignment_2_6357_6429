/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import XmlFile.DishXml;
import XmlFile.TableXml;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author LENOVO
 */
public class PopMessage {
    
     public void display(){
         
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("Enter username/password before pressing login.");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,300,300);
        window.setScene(scene);
        window.showAndWait();
        
    }
     
       public void display2(){
          
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("Something wrong occurred while loading file.");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,300,300);
        window.setScene(scene);
        window.showAndWait();
        
    }
       
       public void display3(){
         
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("username/password are invalid");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,300,300);
        window.setScene(scene);
        window.showAndWait();
        
    }
       public void TableErrorMessage()
       {
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("Unavailable table");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,300,300);
        window.setScene(scene);
        window.showAndWait();
           
       }
      
         public void displayTables(List<TableXml> tables){
        Stage window = new Stage();
        window.setTitle("Available Tables");
        TableView<Table> table;
        //table number coloumn column
        TableColumn<Table,Integer> numberColumn = new TableColumn<>("Table Number");
        numberColumn.setMinWidth(200);
        numberColumn.setCellValueFactory(new PropertyValueFactory<>("tableNumber"));

        //number of seats column
        TableColumn<Table,Integer> numberOfSeatsColumn = new TableColumn<>("Number Of Seats");
        numberOfSeatsColumn.setMinWidth(200);
        numberOfSeatsColumn.setCellValueFactory(new PropertyValueFactory<>("numOfSeats"));

        //smoking column
        TableColumn<Table, String> smokingColumn = new TableColumn<>("Smoking Area");
        smokingColumn.setMinWidth(200);
        smokingColumn.setCellValueFactory(new PropertyValueFactory<>("smoking"));

        table = new TableView<>();
        table.setItems(getProduct(tables));
        table.getColumns().addAll(numberColumn, numberOfSeatsColumn, smokingColumn);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(table);

        Scene scene = new Scene(vBox,700,500);
        window.setScene(scene);
        window.show();
        
    }
         
     public ObservableList<Table> getProduct(List<TableXml> table){
        ObservableList<Table> tables = FXCollections.observableArrayList();
        for(int i=0;i<table.size();i++){
            tables.add(new Table(table.get(i).getNumber(),table.get(i).getNumber_of_seats(), table.get(i).isSmoking() ? "smoking" : "non-smoking" ));
        }
       
        return tables;
    }   
     
      public ObservableList<Dish> getProduct1(List<DishXml> dish){
        ObservableList<Dish> dishes = FXCollections.observableArrayList();
        for(int i=0;i<dish.size();i++){
            dishes.add(new Dish(dish.get(i).getName(),dish.get(i).getType(), dish.get(i).getPrice() ));
        }
       
        return dishes;
    }
       public void displayMenu(List<DishXml> dish){
         
        Stage window = new Stage();
        window.setTitle("Menu");
        TableView<Dish> dishes;
        //table number coloumn column
        TableColumn<Dish,String> nameColumn = new TableColumn<>("Dish Name");
        nameColumn.setMinWidth(200);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        //number of seats column
        TableColumn<Dish,String> typeColumn = new TableColumn<>("Dish Type");
        typeColumn.setMinWidth(200);
        typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));

        //smoking column
        TableColumn<Dish, Integer> priceColumn = new TableColumn<>("Price in LE");
        priceColumn.setMinWidth(200);
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

        dishes = new TableView<>();
        dishes.setItems(getProduct1(dish));
        dishes.getColumns().addAll(nameColumn, typeColumn, priceColumn);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(dishes);

        Scene scene = new Scene(vBox,700,500);
        window.setScene(scene);
        window.show();
        
    }
              public void display4(){
          
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("Enter table number!");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,300,300);
        window.setScene(scene);
        window.showAndWait();
        
    }
              
    public void display5(){
         
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("No available tables at the time");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,300,300);
        window.setScene(scene);
        window.showAndWait();
        
    }
    
    public void display6(){
         
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("Choose a dish first!");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,400,300);
        window.setScene(scene);
        window.showAndWait();
        
    }
    public void display7(){
         
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("This table number is not available. \nPlease check available tables and enter correct table number.");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,500,300);
        window.setScene(scene);
        window.showAndWait();
        
    }public void display8(){
         
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("No reservations at the moment.");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,500,300);
        window.setScene(scene);
        window.showAndWait();
        
    }
         public void display10()
    {
         Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("This username already exists. \nTry another.");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        Scene scene= new Scene(layout,500,300);
        window.setScene(scene);
        window.showAndWait();
        
    }
    public void display9()
    {
                Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("Enter all data required");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,500,300);
        window.setScene(scene);
        window.showAndWait();
    }
    public void display11()
    {
              Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Error");
        Label label=new Label();
        label.setText("Password has to be at least 8 characters!");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,500,300);
        window.setScene(scene);
        window.showAndWait();
    }
    public void display12(){
            Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Confirm");
        Label label=new Label();
        label.setText("Your account has been registered. \nGo back to login.");
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label);
        layout.setAlignment(Pos.CENTER);
        
        Scene scene= new Scene(layout,500,300);
        window.setScene(scene);
        window.showAndWait();
    }
    
   
    
    
}
