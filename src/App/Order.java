/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import java.util.List;

public class Order {
    private String name;
    private int tableNumber;
    private List<String>order1;
    private List<Integer> dishQuantity;
    private double totalMoney;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public List<String> getOrder1() {
        return order1;
    }

    public void setOrder1(List<String> order1) {
        this.order1 = order1;
    }

    public List<Integer> getDishQuantity() {
        return dishQuantity;
    }

    public void setDishQuantity(List<Integer> dishQuantity) {
        this.dishQuantity = dishQuantity;
    }

    public Order(String name,int tablenumber, List<String> order1,List<Integer>nums,double money) {
        this.name = name;
        this.tableNumber = tablenumber;
        this.order1 = order1;
        this.dishQuantity=nums;
        this.totalMoney=money;
    }
    public Order(){
        this.name="";
        this.tableNumber=0;
    }
    
    
    
}
