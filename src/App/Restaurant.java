package App;

import XmlFile.CookerDashboardXml;
import XmlFile.CookerXml;
import XmlFile.Dashboards;
import XmlFile.ManagerDashboardXml;
import XmlFile.ManagerXml;
import XmlFile.RestaurantXml;
import XmlFile.TableXml;
import XmlFile.TablesXml;
import XmlFile.WaiterDashboardXml;
import XmlFile.WaiterXml;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javax.xml.bind.Marshaller;




public class Restaurant extends Application {

    String role;
    Scene scene, sceneCustomer, sceneWaiter, sceneManager, sceneCooker,scenesignin;
    Stage window = new Stage();
    List<Order> orders = new ArrayList<>();
    Order orderClass;
    double totalPrice = 0.0; //total price earned
    List<String> comboBoxValueList = new ArrayList<>();
    List<Integer> spinnerValueList = new ArrayList<>();
    List<Double> tMoney = new ArrayList<>();
    Customer client = new Customer();   
   
    List<TableXml> tableXmlList = new ArrayList<>();
   
    boolean empty = true;

   
    public static void main(String[] args) {

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws JAXBException, FileNotFoundException {

        window = primaryStage;

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10, 10, 10, 10));
        grid.setVgap(10);
        grid.setHgap(2);
        PopMessage message = new PopMessage();
        User exception = new User();

        JAXBContext jaxbContext = JAXBContext.newInstance(RestaurantXml.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        RestaurantXml restaurantXml = (RestaurantXml) unmarshaller.unmarshal(new File("Data.xml"));

        JAXBContext jaxb = JAXBContext.newInstance(Dashboards.class);
        Unmarshaller unmarshaller1 = jaxb.createUnmarshaller();

        Marshaller marshaller1 = jaxb.createMarshaller();
        Dashboards dashboards = (Dashboards) unmarshaller1.unmarshal(new File("Save.xml"));

        //waiter
        WaiterDashboardXml waiterDashboard = new WaiterDashboardXml();
        WaiterXml waiterXml = new WaiterXml();
        List<WaiterDashboardXml> waiterXmlList = new ArrayList<>();
        //cooker
        CookerDashboardXml cookerDashboard = new CookerDashboardXml();
        CookerXml cookerxml = new CookerXml();
        List<CookerDashboardXml> cookerXmlList = new ArrayList<>();
        //manager
        ManagerDashboardXml managerDashboard = new ManagerDashboardXml();
        ManagerXml managerxml = new ManagerXml();
        List<ManagerDashboardXml> managerXmlList = new ArrayList<>();

        

        Label label = new Label("Welcome please enter username and password to start!");
        GridPane.setConstraints(label, 0, 0);

        Label username = new Label("Username");
        GridPane.setConstraints(username, 0, 1);

        TextField usernameText = new TextField();
        GridPane.setConstraints(usernameText, 1, 1);
        usernameText.setPromptText("username");
        usernameText.setMinSize(10, 10);

        Label password = new Label("Password");
        GridPane.setConstraints(password, 0, 2);

        PasswordField passwordText = new PasswordField();
        GridPane.setConstraints(passwordText, 1, 2);
        passwordText.setPromptText("password");
        passwordText.setMinSize(10, 10);

        Button login = new Button("Login");
        GridPane.setConstraints(login, 0, 3);
        
        Button signin = new Button ("SignUp for Client");
        GridPane.setConstraints(signin,0,4);
        signin.setOnAction(e->{
            window.setScene(scenesignin);
        });
        grid.getChildren().add(signin);

        login.setOnAction(e -> {
            role = exception.logIn(usernameText, passwordText, restaurantXml);

            if (role != null) {

                switch (role) {
                    case "Client":
                        window.setScene(sceneCustomer);
                        break;
                    case "Waiter":

                        window.setScene(sceneWaiter);
                        break;
                    case "Manager":

                        window.setScene(sceneManager);
                        break;
                    case "cooker":

                        window.setScene(sceneCooker);
                        break;
                }
            }
            passwordText.clear();
            usernameText.clear();
        });

        grid.getChildren().addAll(label, username, password, usernameText, passwordText, login);
        scene = new Scene(grid, 800, 800);
        
        
         //scene sign up
        GridPane grids = new GridPane();
        grids.setVgap(10);
        grids.setHgap(2);
        grids.setPadding(new Insets(10, 10, 10, 10));
        Label nameLabel2 =new Label("Enter name:");
        GridPane.setConstraints(nameLabel2, 0, 1);
        TextField name3 = new TextField();
        GridPane.setConstraints(name3, 1, 1);
        Label userLabel2 =new Label("Enter username:");
        GridPane.setConstraints(userLabel2, 0, 2);
        TextField username3 = new TextField();
        GridPane.setConstraints(username3, 1, 2);
        Label passLabel2 =new Label("Enter password:");
        GridPane.setConstraints(passLabel2, 0, 3);
        PasswordField password3 = new PasswordField();
        GridPane.setConstraints(password3, 1, 3);
        Button register = new Button("Register");
        register.setOnAction(e->{
            
            Boolean s=client.signUp(name3,username3,password3,restaurantXml,jaxbContext);
            if(s)
            {
               message.display12();
            }
            
            username3.setText("");
            password3.setText("");
            name3.setText("");

        });
         Button backs = new Button("Back");
        backs.setOnAction(e -> {
            window.setScene(scene);
        });
        GridPane.setConstraints(register, 0, 4);
        GridPane.setConstraints(backs, 1, 4);
        grids.getChildren().addAll(backs,register,name3,username3,password3,nameLabel2,passLabel2,userLabel2);
        scenesignin = new Scene(grids, 800, 800);

        //sceneCustomer
        GridPane grid2 = new GridPane();
        grid2.setPadding(new Insets(10, 10, 10, 10));
        grid2.setVgap(10);
        grid2.setHgap(2);
        Label welcome = new Label("Welcome!");
        GridPane.setConstraints(welcome, 0, 0);
        Button menue = new Button("View Menu");
        GridPane.setConstraints(menue, 0, 2);
        menue.setOnAction(e -> {
            message.displayMenu(restaurantXml.getDishes().getDishes());
        });

        Button tables = new Button("View available tables");
        GridPane.setConstraints(tables, 4, 2);
        tables.setOnAction(e -> {
            try {
                message.displayTables(restaurantXml.getTables().getTables());
            } catch (Exception ex) {
                message.display5();
            }

        });
        Label hint = new Label("To reserve:");
        GridPane.setConstraints(hint, 0, 3);
        Label enter = new Label("Enter table number: ");
        GridPane.setConstraints(enter, 0, 4);
        TextField tableNum = new TextField();
        GridPane.setConstraints(tableNum, 2, 4);
        tableNum.setPrefWidth(100);
        tableNum.setPromptText("Enter table number");
        Label order = new Label("Enter Order:");
        GridPane.setConstraints(order, 0, 5);
        ComboBox<String> box = new ComboBox<>();
        GridPane.setConstraints(box, 0, 6);
        box.setPromptText("Choose dish");
        for (int j = 0; j < restaurantXml.getDishes().getDishes().size(); j++) {
            box.getItems().add(restaurantXml.getDishes().getDishes().get(j).getName());
        }

        Spinner<Integer> spinner = new Spinner<>();
        GridPane.setConstraints(spinner, 2, 6);
        SpinnerValueFactory<Integer> valueFactory
                = //
                new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 5, 0);
        spinner.setValueFactory(valueFactory);
        spinner.setEditable(true);

        Label spin = new Label("(Enter number of dishes of this type)");
        GridPane.setConstraints(spin, 3, 6);

        Button add = new Button("Add dish to check");
        GridPane.setConstraints(add, 4, 6);

        add.setOnAction(e -> {
            if (box.getValue() == null) {
                message.display6();
            } else {
                comboBoxValueList.add(box.getValue());
                spinnerValueList.add(spinner.getValue());
                box.setValue(null);
                spinner.getValueFactory().setValue(0);
                
                
            }
        });
        Button reserve = new Button("Reserve");
        GridPane.setConstraints(reserve, 2, 17);
        Label money = new Label(); //label for total money
        reserve.setOnAction(e -> {
            try {
                boolean exists = false;
                int tablenumber = Integer.parseInt(tableNum.getText());
                for (TableXml table : restaurantXml.getTables().getTables()) {
                    if (tablenumber == table.getNumber()) {
                        exists = true;

                    }
                }
                if (exists) {
                    
                        client.Checkout(comboBoxValueList, spinnerValueList, restaurantXml.getDishes().getDishes());
                        double price = client.getPaymentAmount(); //total price for customer

                        
                        orderClass = new Order(exception.getName(), tablenumber, new ArrayList(comboBoxValueList), new ArrayList(spinnerValueList), price);
                        orders.add(orderClass);

                       
                        tableXmlList = new ArrayList<>();
                        for (TableXml Table : restaurantXml.getTables().getTables()) {
                            if (Table.getNumber() != tablenumber) {
                               
                                tableXmlList.add(new TableXml(Table.getNumber(), Table.getNumber_of_seats(), Table.isSmoking()));
                            }
                        }
                        TablesXml newList = new TablesXml();
                        newList.setTables(tableXmlList);
                        restaurantXml.setTables(newList);
                        try {
                            Marshaller marshaller = jaxbContext.createMarshaller();
                            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                            OutputStream output = new FileOutputStream("Data.xml");
                            marshaller.marshal(restaurantXml, output);
                        } catch (JAXBException ex) {
                            System.out.println("error jaxb catch 1");
                        } catch (FileNotFoundException ex) {
                            System.out.println("error file catch 2");
                        }

                        money.setText("Total money to be paid =" + price + "LE");
                        GridPane.setConstraints(money, 3, 10);
                        //saving waiter
                       
                        if (dashboards.getWaiterXml() == null) {
                            waiterDashboard.setPersonName(exception.getName());
                            waiterDashboard.setReservedTableNumber(tablenumber);
                            waiterXmlList.add(waiterDashboard);
                            waiterXml.setWaiterDashboradXml(new ArrayList(waiterXmlList));
                            dashboards.setWaiterXml(waiterXml);
                            
                            try {
                                
                                marshaller1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                                OutputStream output1 = new FileOutputStream("Save.xml");
                                marshaller1.marshal(dashboards, output1);
                            } catch (Exception r) {
                                System.out.println("waiter empty catch " + r);
                            }
                        } else {
                            
                            
                            dashboards.getWaiterXml().getWaiterDashboradXml().add(new WaiterDashboardXml(exception.getName(), tablenumber));
                            waiterXml.setWaiterDashboradXml(new ArrayList(dashboards.getWaiterXml().getWaiterDashboradXml()));
                            dashboards.setWaiterXml(waiterXml);
                            try {
                                
                                marshaller1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                                OutputStream output1 = new FileOutputStream("Save.xml");
                                marshaller1.marshal(dashboards, output1);
                            } catch (Exception w) {
                                System.out.println(w);
                            }
                        }

                        //saving cooker 
                        if (dashboards.getCookerXml() == null) {
                            cookerDashboard.setNumberOfTable(tablenumber);
                            cookerDashboard.setOrdersList(new ArrayList(comboBoxValueList));
                            cookerDashboard.setQuantity(new ArrayList(spinnerValueList));

                            cookerXmlList.add(cookerDashboard);
                            cookerxml.setCookerDashboardXml(new ArrayList(cookerXmlList));
                            dashboards.setCookerXml(cookerxml);
                            try {
                               
                                marshaller1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                                OutputStream output1 = new FileOutputStream("Save.xml");
                                marshaller1.marshal(dashboards, output1);
                            } catch (Exception r) {
                                System.out.println("cooker empty catch" + r);
                            }
                        } else {
                           
                            
                            dashboards.getCookerXml().getCookerDashboardXml().add(new CookerDashboardXml(tablenumber,new ArrayList(comboBoxValueList),new ArrayList(spinnerValueList)));
                            cookerxml.setCookerDashboardXml(new ArrayList(dashboards.getCookerXml().getCookerDashboardXml()));
                            
                            dashboards.setCookerXml(cookerxml);
                            try {
                                marshaller1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                                OutputStream output1 = new FileOutputStream("Save.xml");
                                marshaller1.marshal(dashboards, output1);
                            } catch (Exception w) {
                                System.out.println(w);
                            }
                        }

                        //saving manager 
                        if (dashboards.getManagerXml() == null) {
                            managerDashboard.setTableNumberManager(tablenumber);
                            managerDashboard.setOrderedDishes(new ArrayList(comboBoxValueList));
                            managerDashboard.setDishesQuantity(new ArrayList(spinnerValueList));
                            managerDashboard.setAmount(price);
                            managerDashboard.setNameOfPerson(exception.getName());
                            managerXmlList.add(managerDashboard);
                            managerxml.setManagerDashborad(new ArrayList(managerXmlList));
                            dashboards.setManagerXml(managerxml);
                            try {
                                marshaller1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                                OutputStream output1 = new FileOutputStream("Save.xml");
                                marshaller1.marshal(dashboards, output1);
                            } catch (Exception r) {
                                System.out.println("manager empty catch" + r);
                            }
                        } else {
                          
                            dashboards.getManagerXml().getManagerDashborad().add(new ManagerDashboardXml(exception.getName(),tablenumber,new ArrayList(comboBoxValueList),new ArrayList(spinnerValueList),price));
                            managerxml.setManagerDashborad(new ArrayList(dashboards.getManagerXml().getManagerDashborad()));
                            
                            dashboards.setManagerXml(managerxml);
                            try {
                                marshaller1.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                                OutputStream output1 = new FileOutputStream("Save.xml");
                                marshaller1.marshal(dashboards, output1);
                            } catch (Exception w) {
                                System.out.println(w);
                            }
                        }                        
                        comboBoxValueList.clear();
                        spinnerValueList.clear();

                   

                } else {
                    message.display7();
                }

            } catch (Exception s) {
                
                message.display4();
            }
        });

        Button back = new Button("Log Out");
        GridPane.setConstraints(back, 0, 17);
        back.setOnAction(e -> {
            window.setScene(scene);
            
            money.setText("");
            tableNum.clear();
             box.setValue(null);
             spinner.getValueFactory().setValue(0);
            
            
        });

        grid2.getChildren().addAll(welcome, back, tables, menue, hint, enter, tableNum, order, spinner, box, spin, add, reserve, money);
        sceneCustomer = new Scene(grid2, 900, 800);

        //sceneWaiter
        GridPane grid3 = new GridPane();
        grid3.setPadding(new Insets(10, 10, 10, 10));
        grid3.setVgap(10);
        grid3.setHgap(2);
        Label welcomeW = new Label("Welcome waiter!");
        GridPane.setConstraints(welcomeW, 0, 0);
        Button showReservations = new Button("View Reservations for today");
        GridPane.setConstraints(showReservations, 0, 1);
        List<Label> labels = new ArrayList();

        showReservations.setOnAction(e -> {
            try{
            int count = 0;
            int counter1 = 1;
            for (WaiterDashboardXml waiter : dashboards.getWaiterXml().getWaiterDashboradXml()) {
                Label TableNum = new Label();
                Label tableNum2 = new Label();
                Label personName = new Label();
                Label name = new Label();
                TableNum.setText("Table Number");
                GridPane.setConstraints(TableNum, count, counter1 + 1); 
                tableNum2.setText("" + waiter.getReservedTableNumber());
                GridPane.setConstraints(tableNum2, count + 1, counter1 + 1); 
                personName.setText("Reserved by: ");
                GridPane.setConstraints(personName, count, counter1 + 2); 
                name.setText("" + waiter.getPersonName());
                GridPane.setConstraints(name, count + 1, counter1 + 2);  
                counter1 = counter1 + 2;
                labels.add(name);
                labels.add(tableNum2);
                labels.add(TableNum);
                labels.add(personName);
                grid3.getChildren().addAll(name, tableNum2, TableNum, personName);

            }
            }catch(Exception z){
                message.display8();
            }
        });

        Button back1 = new Button("Log Out");
        GridPane.setConstraints(back1, 5, 1);
        back1.setOnAction(e -> {
            window.setScene(scene);

            grid3.getChildren().removeAll(labels);
        });
        grid3.getChildren().addAll(welcomeW, back1, showReservations);
        sceneWaiter = new Scene(grid3, 800, 800);

        //sceneManager
        GridPane grid4 = new GridPane();
        grid4.setPadding(new Insets(10, 10, 10, 10));
        grid4.setVgap(10);
        grid4.setHgap(2);
        Label welcomeM = new Label("Welcome manager!");
        GridPane.setConstraints(welcomeM, 0, 0);

        Button viewmenu = new Button("view menu");

        List<Label> managerlist = new ArrayList();
        viewmenu.setOnAction(e -> {
            message.displayMenu(restaurantXml.getDishes().getDishes());
        });
        GridPane.setConstraints(viewmenu, 0, 2);
        Button ordersdetails = new Button("Show Order details");
        ordersdetails.setOnAction(e -> {
            try{
          
            int count = 0;
            int count5 = 3;
            for (ManagerDashboardXml manager : dashboards.getManagerXml().getManagerDashborad()) {
                Label clientname = new Label("Client Name:    " + manager.getNameOfPerson());
                GridPane.setConstraints(clientname, count, count5);
                count5++;
                managerlist.add(clientname);
                Label tablenum = new Label("Table Number:   " + manager.getTableNumberManager());
                GridPane.setConstraints(tablenum, count, count5);
                count5++;
                managerlist.add(tablenum);
                Label price = new Label("Paid Amount:    " + manager.getAmount() + " LE");
                GridPane.setConstraints(price, count, count5);
                count5++;
                managerlist.add(price);
                Label x = new Label("Order: ");
                GridPane.setConstraints(x, count, count5);
                managerlist.add(x);
                totalPrice+= manager.getAmount();
                for (int j = 0; j < manager.getOrderedDishes().size() ; j++) {
                    Label TableOrders2 = new Label("" + manager.getOrderedDishes().get(j) + "     Quantity:  " + manager.getDishesQuantity().get(j));
                    GridPane.setConstraints(TableOrders2, count + 1, count5);
                    managerlist.add(TableOrders2);
                    grid4.getChildren().add(TableOrders2);
                    count5 = count5 + 1;
                }
                grid4.getChildren().addAll(clientname, tablenum, price, x);
            }
           
            Label totalmoney = new Label("Total earned money= " + totalPrice + " LE");
            totalPrice= 0.0;
            managerlist.add(totalmoney);
            GridPane.setConstraints(totalmoney, 2, 0);
            grid4.getChildren().add(totalmoney);
        }catch(Exception w){
            message.display8();
        }
        });
        Button back2 = new Button("Log Out");
        GridPane.setConstraints(back2, 10, 2);
        back2.setOnAction(e -> {
            window.setScene(scene);
            grid4.getChildren().removeAll(managerlist);
            
        });
        grid4.getChildren().addAll(welcomeM, back2, ordersdetails,viewmenu);
        sceneManager = new Scene(grid4, 800, 800);

        //sceneCooker
        GridPane grid5 = new GridPane();
        grid5.setPadding(new Insets(10, 10, 10, 10));
        grid5.setVgap(10);
        grid5.setHgap(2);
        
        
       
        
        Label welcomeC = new Label("Welcome cook!");
        GridPane.setConstraints(welcomeC, 0, 0);
        Button showOrders = new Button("Show today's orders");
        GridPane.setConstraints(showOrders, 0, 1);
        List<Label> labelcooker = new ArrayList();
        showOrders.setOnAction(e -> {
            try{
            int counter = 0;
            int counter1 = 1;
            for (CookerDashboardXml cooker : dashboards.getCookerXml().getCookerDashboardXml()) {
                Label TableNumber = new Label("Table Number: ");
                GridPane.setConstraints(TableNumber, counter, counter1 + 1);  
                labelcooker.add(TableNumber);
                Label TableOrders = new Label("Table Orders: ");
                GridPane.setConstraints(TableOrders, counter, counter1 + 2);  
                labelcooker.add(TableOrders);
                Label TableNumber2 = new Label("" + cooker.getNumberOfTable());
                GridPane.setConstraints(TableNumber2, counter + 1, counter1 + 1); 
                labelcooker.add(TableNumber2);
                for (int j = 0; j < cooker.getOrdersList().size(); j++) {
                    
                    Label TableOrders2 = new Label("" + cooker.getOrdersList().get(j) + "   Quantity:  " + cooker.getQuantity().get(j));
                    GridPane.setConstraints(TableOrders2, counter + 1, counter1 + 2);
                    labelcooker.add(TableOrders2);
                    grid5.getChildren().add(TableOrders2);
                    counter1 = counter1 + 1;
                }
                counter1 = counter1 + 2;
                grid5.getChildren().addAll(TableNumber, TableOrders, TableNumber2);
            }
            }catch(Exception z){
                message.display8();
            }
        });
        Button back3 = new Button("Log Out");
        GridPane.setConstraints(back3, 5, 1);
        back3.setOnAction(e -> {
            window.setScene(scene);
            grid5.getChildren().removeAll(labelcooker);
        });
        grid5.getChildren().addAll(welcomeC, back3, showOrders);
        sceneCooker = new Scene(grid5, 800, 800);

        window.setScene(scene);
        window.setTitle("RESTAURANT");
        window.show();
    }

}
