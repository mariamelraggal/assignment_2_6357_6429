/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

/**
 *
 * @author LENOVO
 */
public class Table {
    private int tableNumber;
    private int numOfSeats;
    private String smoking;
    private boolean reserved;
    private String TableNum;

    public Table(int tableNumber, int numOfSeats, String smoking) {
        this.tableNumber = tableNumber;
        this.numOfSeats = numOfSeats;
        this.smoking = smoking;
    }
    
    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getTableNum() {
        return TableNum;
    }

    public void setTableNum(String TableNum) {
        this.TableNum = TableNum;
    }

    public int getNumOfSeats() {
        return numOfSeats;
    }

    public void setNumOfSeats(int numOfSeats) {
        this.numOfSeats = numOfSeats;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

   
    
}
